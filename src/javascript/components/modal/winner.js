import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage, createFighterInfo } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const winner = createWinnerFighter(fighter);
  showModal({ title: `This is winner!`, bodyElement: createWinnerFighter(fighter) });
  // call showModal function
}

function createWinnerFighter(fighter) {
  const fighterImg = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);
  const wrapper = createElement({ tagName: 'div' });
  wrapper.append(fighterImg, fighterInfo);
  return wrapper;
}
