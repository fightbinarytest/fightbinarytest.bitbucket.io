import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const fighters = new Fights(firstFighter, secondFighter, controls, winner);
    const listener = new Listener(controls, fighters.checkHitsBind);
    function winner(fighter) {
      const winner = fighter._id === firstFighter._id ? firstFighter : secondFighter;
      listener.removeListener();
      resolve(winner);
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const power = hitPower - blockPower;
  return power > 0 ? power : 0;
  // return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
  // return block power
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

class Fights {
  critAttackCoefficient = 2;
  critTimeot = 10 * 1000;
  constructor(leftFighter, rightFighter, controls, cb) {
    this.leftFighter = {
      ...leftFighter,
      startHealth: leftFighter.health,
      indicator: document.getElementById('left-fighter-indicator'),
      codeBlock: controls.PlayerOneBlock,
      codeAttack: controls.PlayerOneAttack,
      combineKeys: controls.PlayerOneCriticalHitCombination,
    };
    this.rightFighter = {
      ...rightFighter,
      startHealth: rightFighter.health,
      indicator: document.getElementById('right-fighter-indicator'),
      codeBlock: controls.PlayerTwoBlock,
      codeAttack: controls.PlayerTwoAttack,
      combineKeys: controls.PlayerTwoCriticalHitCombination,
    };
    this.controls = controls;
    this.checkHitsBind = this.checkHits.bind(this);
    this.cb = cb;
  }
  checkHits(codes) {
    [
      [this.leftFighter, this.rightFighter],
      [this.rightFighter, this.leftFighter],
    ].forEach(([attacker, defender]) => {
      if (codes.has(attacker.codeAttack) && !codes.has(attacker.codeBlock) && !codes.has(defender.codeBlock)) {
        defender.health -= getDamage(attacker, defender);
      }
      if (attacker.combineKeys.every((code) => codes.has(code)) && !attacker.crit) {
        defender.health -= this.critAttackCoefficient * attacker.attack;
        attacker.crit = true;
        const timeout = setTimeout(() => {
          clearTimeout(timeout);
          attacker.crit = false;
        }, this.critTimeot);
      }
      if (defender.health <= 0) {
        this.cb(attacker);
      }
    });
    this.changeIndicators();
  }
  changeIndicators() {
    [this.leftFighter, this.rightFighter].forEach((fighter) => {
      const startHelth = fighter.startHealth;
      const width = 100 - ((startHelth - fighter.health) / startHelth) * 100;
      fighter.indicator.style.width = `${width < 0 ? 0 : width}%`;
    });
  }
}

class Listener {
  constructor(keysCode, cb) {
    this.allKeysCode = [
      ...Object.values(keysCode).reduce((newKeys, key) => [...newKeys, ...(Array.isArray(key) ? key : [key])], []),
    ];
    this.keysPress = new Set();
    this.body = document.querySelector('body');
    this.handlerKeyDownBind = this.handlerKeyDown.bind(this);
    this.handlerKeyUpBind = this.handlerKeyUp.bind(this);
    this.cb = cb;
    this.addListener();
  }
  addListener() {
    this.body.addEventListener('keydown', this.handlerKeyDownBind);
    this.body.addEventListener('keyup', this.handlerKeyUpBind);
  }
  removeListener() {
    this.body.removeEventListener('keydown', this.handlerKeyDownBind);
    this.body.removeEventListener('keyup', this.handlerKeyUpBind);
  }
  handlerKeyDown(event) {
    const code = event.code;
    if (!this.allKeysCode.includes(code) || this.keysPress.has(code)) {
      return;
    }
    event.preventDefault();
    this.keysPress.add(code);
    this.cb(this.keysPress);
  }
  handlerKeyUp(event) {
    this.keysPress.delete(event.code);
  }
}
