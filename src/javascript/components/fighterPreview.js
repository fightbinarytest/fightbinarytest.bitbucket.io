import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (!fighter) {
    return fighterElement;
  }
  const fighterImg = createFighterImage(fighter);
  const fighterWrapper = createElement({ tagName: 'div', className: 'fighter-info___root' });
  const fighterInfo = createFighterInfo(fighter);
  fighterWrapper.append(fighterImg, fighterInfo);
  fighterElement.append(fighterWrapper);

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

const label = {
  name: 'Name',
  attack: 'Attack',
  defense: 'Defense',
  health: 'Health',
};

export function createFighterInfo(fighter) {
  const { _id, source, ...restFighterData } = fighter;
  const wrapper = createElement({ tagName: 'div', className: 'fighter-info___content' });
  const infoElements = Object.entries(restFighterData).map(([key, value]) => {
    const line = createElement({ tagName: 'div', className: 'fighter-info-line___content' });
    const titleElement = createElement({ tagName: 'span' });
    titleElement.innerHTML = `${label[key]}`;
    const valueElement = createElement({ tagName: 'span' });
    valueElement.innerHTML = `${value}`;
    line.append(titleElement, valueElement);
    return line;
  });
  wrapper.append(...infoElements);
  return wrapper;
}
